# Markdown

## What is it?

*Markdown* is a lightweight markup language with plain text formatting syntax.

Its design allows it to be converted to many output formats, but the original tool by the same name only supports HTML.

## Where I can use it?

*Markdown* is often used to format readme files, for writing messages in online discussion forums, and to create rich text using a plain text editor.  

You can use *Markdown* also to create fast and secure websites, like in [Jekyll.rb](https://jekyllrb.com/), [sample here](http://cristianocasella.com).

[Next slide](/slides/1.md)
