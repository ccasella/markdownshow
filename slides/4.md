## Listing elements
You can have ordered or unordered lists

You can use `*`,`-` or `+` for unordered list and `1.` (or any other number) for ordered list, and you can mix them

```
1. Main argument
4. Secondary argument
   6. First sub-argument
      - First point
      - Second point
   1. Second sub-argument
1. Last argument
10. Q&A
```

1. Main argument
4. Secondary argument
   6. First sub-argument
      - First point
      - Second point
   1. Second sub-argument
1. Last argument
10. Q&A

## Hyperlinks and Images
You can include links (absolute and relative) or image:

`[Google](https://google.com)`

[Google](https://google.com)

Adding a `!` at the begin will show the image:

`![Tanuki](https://media.mnn.com/assets/images/2016/01/adorabletanuki.jpg.653x0_q80_crop-smart.jpg)`

![Tanuki](https://media.mnn.com/assets/images/2016/01/adorabletanuki.jpg.653x0_q80_crop-smart.jpg)

## Code and Syntax Highlighting
You can use inline or multilines quotes for code, also for syntax highlighting:

This is a `` `variable` ``.

This is a `variable`.

````
```python
tanuki = "Tanuki is the alternative name of the Japanese raccoon dog (Nyctereutes procyonoides viverrinus)"
print tanuki
```
````

```python
tanuki = "Tanuki is the alternative name of the Japanese raccoon dog (Nyctereutes procyonoides viverrinus)"
print tanuki
```

Look at the code of this page, there are some tricks for the escaping of special chars.

## Quotes
You can quote a text with `>` at the begin of each line like in many e-mail readers:

```
> You can't trust a text just because there is a name written below  
> *Albert Einstein*
```

> You can't trust a text just because there is a name written below  
> *Albert Einstein*

[Next Slide](/slides/5.md)
